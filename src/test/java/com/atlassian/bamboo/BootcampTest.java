package com.atlassian.bamboo;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for Bootcamp.
 */
public class BootcampTest extends TestCase {
    /**
     * Tests the return value of the get message function.
     */
    public void testGetMessage() {
        Bootcamp bootcamp = new Bootcamp();

        assertEquals("Hello Bootcampers", bootcamp.getWelcomeMessage());
    }
}
