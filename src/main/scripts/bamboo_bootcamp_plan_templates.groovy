plan(key:'BUIL',name:'Build',description:'Builds and packages the tested code.', enabled:'false') {
   project(key:'BOOT',name:'Bootcamp')

   repository(name:'bamboo-bootcamp')

   stage(name:'Build Stage',description:'Builds the application.') {
      job(key:'JOB1',name:'Build Job',description:'Builds and packages the code.') {
         artifactDefinition(name:'Bootcamp',location:'target',pattern:'*.jar', shared:'true')

         task(type:'checkout',description:'Checkout Default Repository') {
            repository(name:'bamboo-bootcamp')
         }

         task(type:'maven3',goal:'package',mavenExecutable:'Maven 3.2', buildJdk:'JDK',hasTests:'true')
      }
   }

   stage(name:'Demo Stage',description:'Demos the application.', manual:'true') {
      job(key:'DJ',name:'Demo Job',description:'Demos the tested and built application.') {
         artifactSubscription(name:'Bootcamp')

         task(type:'script',description:'Executes the main class contained within the jar',
            scriptBody:'java -cp bamboo-bootcamp-1.0-SNAPSHOT.jar com.atlassian.bamboo.Bootcamp')

      }
   }

   dependencies(triggerForBranches:'true')

   permissions() {
      user(name:'admin',permissions:'read,write,build,clone,administration')

      anonymous(permissions:'read')

      loggedInUser(permissions:'read')

   }
}

plan(key:'TES',name:'Tests',description:'Plan containing tests.', enabled:'false') {
   project(key:'BOOT',name:'Bootcamp')

   repository(name:'bamboo-bootcamp')

   variable(key:'tests',value:'BootcampTest#testGetMessage')

   stage(name:'Test Stage',description:'Tests the application.') {
      job(key:'JOB1',name:'Test Job',description:'Runs the tests for the code.') {
         task(type:'checkout',description:'Checkout Default Repository') {
            repository(name:'bamboo-bootcamp')
         }

         task(type:'maven3',description:'Runs the unit tests',
            goal:'clean test -Dtest=${bamboo.tests}',mavenExecutable:'Maven 3.2',
            buildJdk:'JDK',hasTests:'true')
      }
   }

   dependencies(triggerForBranches:'true') {
      childPlan(planKey:'BOOT-BUIL')
   }

   permissions() {
      user(name:'admin',permissions:'read,write,build,clone,administration')

      anonymous(permissions:'read')

      loggedInUser(permissions:'read')
   }
}


deployment(name:'Bootcamp',planKey:'BOOT-BUIL',description:'Demonstrates deployments in bamboo.') {
   versioning(version:'release-1',autoIncrementNumber:'true')

   environment(name:'Staging') {
      task(type:'cleanWorkingDirectory')

      task(type:'artifactDownload',description:'Download release contents', planKey:'BOOT-BUIL') {
         artifact(name:'all artifacts',localPath:'.')
      }

      task(type:'script',
            scriptBody:'java -cp bamboo-bootcamp-1.0-SNAPSHOT.jar com.atlassian.bamboo.Bootcamp ${bamboo.deploy.environment}')
   }
}
