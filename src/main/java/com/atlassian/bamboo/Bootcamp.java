package com.atlassian.bamboo;

/**
 * Bootcamp class application.
 */
public class Bootcamp {
    /**
     * Entry point for the application.
     *
     * @param args the arguments provided by the user.
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            System.out.println("Arguments provided from the environment:");

            for (String arg : args) {
                System.out.println(arg);
            }
        }

        new Bootcamp().run();
    }

    /**
     * Runs the application.
     */
    public void run() {
        System.out.println(getWelcomeMessage());
    }

    /**
     * @return the welcome message
     */
    public String getWelcomeMessage() {
        return "Hello Bootcampers";
    }
}
